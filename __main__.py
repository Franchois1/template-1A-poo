"""__main__.py

To try the ptd package
"""
from ptd import *

"""
step_internet_load = InternetLoad('incid_rea')
step_chargement = Load("data/covid-hospit-incid-reg-2021-03-03-17h20.csv")
step_conversion = ApplyToColomn(['numReg', 'incid_rea'], int)# converti les colonnes en entier
step_date = ApplyToColomn(["jour"], ToolBox.str_to_date)# converti la colonne jour en date
step_moy = Mean(['numReg', 'incid_rea', 'truc']) # calcul des moyennes
step_var = Variance(['numReg', 'incid_rea']) # calcul des variances 
step_k_means = K_means(['numReg', 'incid_rea'],5,15) #k_means sur les données
step_group_by_day = GroupBy("jour", sum)
step_del_reg = SelectVar(['jour', 'incid_rea','chouette'])

pipeline_import = Pipeline([step_chargement, step_conversion, step_date])
pipe_daily_incid = Pipeline([step_del_reg, step_group_by_day])
pipeline_import[0] = step_internet_load

print(pipeline_import)

donnees = pipeline_import.run()
print(donnees)

moyennes = step_moy.run(donnees)
var = step_var.run(donnees)
k_means = step_k_means.run(donnees)


print(k_means)

day_to_day_incid = pipe_daily_incid.run(donnees)
print(SortBy('jour', True).run( day_to_day_incid))
"""
chargement = InternetLoad('services')
del_col = SelectVar(['dep', 'nb'])
conversion = ApplyToColomn(['nb'], int)
sort = SortBy('dep', descending=True)
agreg = GroupBy("dep", sum)
mapping = Map('nb', 'bellecarte.jpg')

prep = Pipeline([chargement, del_col, conversion, agreg, mapping])

donnees = prep.run()

print(donnees)

#cp = CartoPlot()

#d = ToolBox.dict_writer_to_map(donnees.col('dep'), donnees.col('nb'))
#fig = cp.plot_dep_map(data=d, show_name=False, x_lim=(-6, 10), y_lim=(41, 52))
#fig.show()
#input()


donnees = pipeline_import.run()
print(donnees)
