"""exemples_rapport.py

ce script de répondre aux questions d'analyses posées dans le rapport :
1 — Quel est le nombre total d’hospitalisations dues au Covid-19 ?

2 — Quel est le nombre total d’hospitalistations dues au Covid-19 par 
    departements ?

3 — Combien de nouvelles hospitalisations ont eu lieu ces 7 derniers jours 
    dans chaque département ?

4 — Comment évolue la moyenne des nouvelles hospitalisations journalières 
    de cette semaine par rapport à celle de la semaine dernière ?

5 — Quel est le résultat de k-means avec k = 3 sur les données des départements 
    du mois de Janvier2021,lissées avec une moyenne glissante de 7 jours ?

6 — Combien de nouvelles admissions en réanimation ont eu lieu pendant la 
    semaine suivant les vacancesde la Toussaint de 2020 ?
"""
from datetime import date as dt
from datetime import timedelta
from numpy import *

from ptd import *

# 1st Question
load_flux = InternetLoad("flux")
int_cast = ApplyToColomn(var=["incid_hosp", "incid_rea", "incid_dc", "incid_rad"], function=int)

pipeline = Pipeline([load_flux, int_cast])
Q1_table = pipeline.run()
nb_hosp_tt = sum(Q1_table.col('incid_hosp'))
print(f"Q1 : Nombre total hospitalisations : {nb_hosp_tt}")

# 2nd Question
drop_day = SelectVar(['dep',"incid_hosp", "incid_rea", "incid_dc", "incid_rad"])
dep_agreg = GroupBy('dep', agregation=sum)
pipeline.extend([drop_day, dep_agreg])
Q2_table = pipeline.run()
print("Question 2 : nb tot hosp pasr dep")
print(Q2_table)

# 3rd Question
def is_last_week(date: dt):
    return date > dt.today() - timedelta(days=8)
filter_last_week = Filter('jour', is_last_week)
cast_date = ApplyToColomn(['jour'], function=ToolBox.str_to_date)

pipeline[2] = cast_date
pipeline.insert(3, filter_last_week)
pipeline.insert(4, drop_day)
Q3_table = pipeline.run()
print("Question 3 : nombre de nouvelles hosp sur les 7 derniers jours par dep")
print(Q3_table)

#4th Question
def is_2weeks_ago(date: dt):
    return (dt.today() - timedelta(days = 7) > date 
            and date > dt.today() - timedelta(days=15))
filter_2weeks_ago = Filter('jour', is_2weeks_ago)
drop_dep = SelectVar(['jour',"incid_hosp", "incid_rea", "incid_dc", "incid_rad"])
daily_mean = GroupBy('jour', sum)

pipeline[4] = drop_dep
pipeline[5] = daily_mean
Q4a_table = pipeline.run() #total journalier pour les 7 derniers jours 
pipeline[3] = filter_2weeks_ago
Q4b_table = pipeline.run() #total journalier pour la semaine d'avant
mean_last_week = ToolBox.mean(Q4a_table.col('incid_hosp'))
mean_2weeks_ago = ToolBox().mean(Q4b_table.col('incid_hosp'))
print("Question 4 : Nombre de nouvelles hospitalisation en france : ")
print(f"Moyenne journalière cette semaine : {mean_last_week}")
print(f"moyenne journalière pour la semaine d'avant : {mean_2weeks_ago}")

#5th Question
"""
Quel est le résultat de k-means avec k = 3 sur les données des départements 
    du mois de Janvier2021,lissées avec une moyenne glissante de 7 jours ?

on va pas pouvoir faire de moyenne glissante par département
du coup on modifie la question :
Quel est le résultat de k-means (k=3) sur les moyennes des variables pour les 
département pour le mois de Janvier 2021.

il faut 
 - convertir tout ce qui doit être converti (int et date)
 - filter janv 2021
 - regrouper par dep (en calculant la moeynne):
    - drop les vairiables jour
    - group by 
 - joindre les tables par dep
 - k-means
"""

pipeline_flux = Pipeline([
    InternetLoad("flux"),
    ApplyToColomn(["incid_rea", "incid_hosp", "incid_rad", "incid_dc"], int),
    ApplyToColomn(["jour"], ToolBox.str_to_date),
    Filter("jour", lambda x: (x.month==1 and x.year==2021)),
    SelectVar(['dep',"incid_rea", "incid_hosp", "incid_rad", "incid_dc"]),
    GroupBy('dep')
])

pipeline_stock = Pipeline([
    InternetLoad("stock"),
    ApplyToColomn(["hosp", "rea", "rad", "dc"], int),
    ApplyToColomn(['jour'], ToolBox.str_to_date),
    Filter("jour", lambda x: (x.month==1 and x.year==2021)),
    SelectVar(['dep',"rea", "hosp", "rad", "dc"]),
    GroupBy('dep')
])

pipeline_services = Pipeline([
    InternetLoad("services"),
    ApplyToColomn(["nb"], int),
    ApplyToColomn(['jour'], ToolBox.str_to_date),
    Filter("jour", lambda x: (x.month==1 and x.year==2021)),
    SelectVar(['dep',"nb"]),
    GroupBy('dep')
])

tab_flux = pipeline_flux.run()
tab_stock = pipeline_stock.run()
tab_services = pipeline_services.run()

join_kMeans = Pipeline([
    Join(tab_stock, 'dep'),
    Join(tab_services, 'dep'),
    K_means(["incid_rea", "incid_hosp", "incid_rad", "incid_dc", 
                "hosp", "rea", "rad", "dc", 'nb'], n_classes=3), 
    Map("Class", 'macarte.jpg')
])

Q5_tab = join_kMeans.run(tab_flux)
