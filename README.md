# ptd
Projet de traitement de données 💾

## Introduction
Projet de première année, codé en Python, ayant pour but la création d'un paquet. Ce paquet permettra de traiter les données covid disponibles sur : [data.gouv.fr](https://www.data.gouv.fr/fr/pages/donnees-coronavirus)

## Description du repertoire 
 - `data/` contient les tables que l'on peut retrouver sur data.gouv.fr
 - `metadata/` contient les metadonées que l'on peut aussi retrouver sur data.gouv.fr
 - `pdt/` correspond au paquet Python en lui même
 - `mapper/` paquet permettant la réalisation de cartes, utilisé par `ptd/`
 - `README.md` le présent fichier
 - `__main__.py` script Python contenant des exemples d'utilisation du paquet `ptd`
 - `exemple_rapport` de même, un script avec des exemples
 - `test.py` script contenant un test unitaire

## Liens utiles : 
- [moodle](https://foad-moodle.ensai.fr/course/view.php?id=149)
- [données](https://www.data.gouv.fr/fr/pages/donnees-coronavirus))
