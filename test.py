from unittest import TestCase
from random import random
import unittest
import pandas as pd

from ptd import *

class ptdTest(TestCase):
    def test_means(self):
        data = []
        for i in range(10):
            data.append([random(), random()])

        my_table = Table(headers = ["a", "b"], data=data)
        X = pd.DataFrame(data=my_table.data, columns=my_table.headers)

        ptd_means = Mean(my_table.headers).run(my_table)
        panda_means = X.mean()

        self.assertEqual(list(panda_means), ptd_means[0])

unittest.main()