"""package ptd

ptd - projet de traitement de donnees

Gives a pipeline constructor, basic operations and a toolbox to
process tables

Importing this package with "from ptd import *" defines the following names :
 - Pipeline 
 - Table
 - ToolBox
It also defines the names of the subpackages 'estimation' and 'transformation'
See their documentations for further information

Modules
-------
table
    contains class Table
pipeline
    contains class Pipeline
operation 
    contains class Operation (abstract class)
table 
    contains class Table
toolBox
    contains class ToolBox

Sub-packages
------------
estimation
    contains modules with operations of type estimation
transformation
    contains modules with operations of type transformation
"""

from .operation import Operation
from .pipeline import Pipeline
from .table import Table
from .toolBox import ToolBox

import ptd.transformation
from .transformation import *
import ptd.estimation
from .estimation import *

__all__ = [
    "Pipeline",
    "Table",
    "ToolBox"
] + estimation.__all__ + transformation.__all__