"""module ptd.ToolBox

Definition of class 'ToolBox'
"""

from datetime import date
from .table import Table
import math

class ToolBox():
    """ToolBox

    Contains static methods for the ptd package 

    Methods
    -------
    mean(data: Iterable[int, float]) -> float :
        mean of a list
    variance(data: Iterable[int, float]) -> float : 
        variance of a list
    str_to_date(input: str) -> date : 
        convert a string into a data type (from datetime package)
    moving_average(data: Iterable[int, float], period: int) -> list[float] :
        moving average on a list for a period 
    euclidian(a,b) :
        Euclidean distance between a and b
    new_col(table : Table, new_var : str, new_data: list) -> Table:
        Table with a new column added to it
    """

    @staticmethod
    def mean(data:[])->float:
        """mean of the data given as parameter

        compute the mean of data, data sould have appropriate type 

        Parameters
        ----------
        data : Iterable[numeric]
            Iterable object containing numeric values (int or float)

        Returns
        -------
        float
            the mean 
        """
        return sum(data)/len(data)
    
    @staticmethod
    def variance(data:[])->float:
        """variance of the data given as parameter

        compute the varaince of data, data sould have appropriate type 

        Parameters
        ----------
        data : Iterable[numeric]
            Iterable object containing numeric values (int or float)

        Returns
        -------
        float
            the variance
        """
        mean = ToolBox.mean(data)
        var = 0
        for i in data:
            var += (i - mean)**2
        return var/len(data)
    
    @staticmethod
    def str_to_date(input: str) -> date:
        """convert dates from string to 'date' type

        Parameters
        ----------
        input : str
            string to convert into a 'date', should be "YYYY-MM-DD"

        Returns
        -------
        date
            the date type corresponding to the entry
        """
        input = input.split('-')
        input = [int(i) for i in input]
        return date(input[0], input[1], input[2])
    
    @staticmethod
    def moving_average(data:[], period:int)->list:
        """moving average of data for periods

        uses the mean method of toolbox

        Parameters
        ----------
        data : list
            elements should be numeric (float or int)
        period : int
            period to compute the moving mean

        Returns
        -------
        list
            list containing the means

        Examples
        --------
        >>> a = [1,2,3,1,2,3,1,2,3]
        >>> ToolBox.moving_average(a, 3)
        [2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0]
        >>> a = [1, 3, 5, 7, 5, 10, 9, 8, 10]
        >>> ToolBox.moving_average(a,7)
        [5.714285714285714, 6.714285714285714, 7.714285714285714]
        """
        result = []
        for i in range(len(data)-period+1):
            result.append(ToolBox.mean(data[i:i+period]))
        return result

    @staticmethod
    def euclidian(a,b) : 
        """Euclidean distance between 2 vectors

        if a and b don't have same dimention (lenght) an IndexError
        is raised

        Parameters 
        ----------
        a : list
            vector number 1 
        b : list
            vector number 2 

        returns 
        -------
        float 
            euclidian distance
        """
        
        if len(a) != len(b):
            raise IndexError("the vectors do not have the same lenght")
        sum=0
        for i in range(len(a)):
            sum = sum + (a[i]-b[i])**2
        dist = math.sqrt(sum)
        return dist 

    @staticmethod
    def new_col(table : Table, new_var : str, new_data: list) -> Table:
        """return the table given as parameter with a column added to it

        Doesn't modify the given Table 'table'
        Raise an IndexError if lenght of 'new_data' doesn't match lenght
        of 'table'

        Parameters
        ----------
        table : Table
            Table to extend
        new_var : str
            name of the variable to add
        new_data : list
            data to add to the rows of table

        Returns
        -------
        Table
            table with one more row
        """
        if len(table) != len(new_data):
            raise IndexError(
                "Lenght of the column doesn't match lenght of table")
        
        new_headers = table.headers[:]
        new_headers.append(new_var)

        new_table = []
        for i in range(len(table)):
            new_table.append(table[i][:])
            new_table[i].append(new_data[i])
        
        return Table(new_headers, new_table)
    