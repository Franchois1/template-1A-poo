"""module ptd.transformation.select_var

Definition of class SelectVar
"""

from ..table import Table
from .transformation import Transformation
from .transpose import Transpose

class SelectVar(Transformation):
    """To select a variable

    Parameters
    ----------
    var : str
        the variable selected in the data
    
    Attributes 
    ----------
    var : str
        the variable selected in the data
    """
    
    def __init__(self, var: str):
        """Constructor

        Parameters
        ----------
        var : iterable [str] 
            the variable selected in the data
        """
        self.var = var

    def run(self, data:Table) -> Table:
        """run the selection of the selected variable

        Does not modify the table given as parameter
    
        Parameters
        ----------
        data : Table
            The initial table on which we select variables

        Returns
        -------
        Table
            The columns of the selected variables are returned as a table. 
           
        """
        new_data = []
        new_headers = []
        
        indices = []
        for variable in self.var:
            if variable in data.headers:
                indices.append(data.headers.index(variable))
                new_headers.append(variable)
        
        for i in range(len(data)):
            row = []
            for j in indices:
                row.append(data[i][j])
            new_data.append(row)

        return Table(new_headers, new_data)
    
    def __str__(self):
        """String representation of the operation

        Returns 
        -------
        str : 
            The representation

        Example 
        -------
        >>> m1 = Select_var("Number of deaths in the last 24 hours")
        >>> __str__(m1)
        'select variables : Number of deaths in the last 24 hours'
        
        """
        return(f"Select variables : '{', '.join(self.var)}'")
