"""module ptd.transformation.save

Definition of class SaveAsCsv
"""

from .transformation import Transformation
from ..table import Table

class SaveAsCsv(Transformation):
    """To save table as csv files

    Parameters
    ----------
    path : str
        where to save the table

    Atributes
    ---------
    path : str
        where to save the table
    """

    def __init__(self, path:str):
        """constructor

        Parameters
        ----------
        path : str
            where to save the table
        """
        self.path = path
    
    def run(self, input_table: Table) -> Table:
        """compute the writing of the csv file

        Uses the save_as_csv method from Table object
        If file given in path exist, it is overwritten
        Else it is created

        Parameters
        ----------
        input_table : Table
            The table to be saved

        Returns
        -------
        Table
            The input table, unmodified
        """
        input_table.save_as_csv(self.path)
        return input_table

    def __str__(self) -> str:
        """String representation of the operation

        Returns
        -------
        str
            The representation
        """
        return f'Save table in : {self.path}'