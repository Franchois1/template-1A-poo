"""subpackage ptd.transformation

This sub-package defines the abstract class transformation and its subclasses
An transformation is an operation(superclass) wich is meant to modify a Table

When ptd package is imported trough 'from ptd import *' the following
names are defined :
 - ApplyToColomn
 - InternetLoad
 - Filter
 - GroupBy
 - ImportJSON
 - Join
 - Load
 - Map
 - SaveAsCsv
 - SelectVar
 - SortBy
 - Transpose
 
Modules
-------
apply
    contains class ApplyToColomn
eLoad
    contains class InternetLoad
filter
    contains class Filter
groupby 
    contains class GroupBy
importjson
    contains class ImportJSON
join
    contains class Join
load
    contains class Load
map
    contains class Map
save
    contains class SaveAsCsv
select_var
    contains class SelectVar
sort
    contains class SortBy
transformation 
    contains class Transformation (abstract class)
transpose 
    contains class Transpose
"""

from .apply import ApplyToColomn
from .eLoad import InternetLoad
from .filter import Filter 
from .groupby import GroupBy
from .importjson import ImportJSON
from .join import Join
from .load import Load 
from .map import Map
from .save import SaveAsCsv
from .select_var import SelectVar
from .sort import SortBy
from .transpose import Transpose

__all__ = [
    "ApplyToColomn",
    "InternetLoad",
    "Filter",
    "GroupBy",
    "ImportJSON",
    "Join",
    "Load",
    "Map",
    "SaveAsCsv",
    "SelectVar",
    "SortBy",
    "Transpose"
]