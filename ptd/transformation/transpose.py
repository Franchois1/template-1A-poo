"""transpose.py

Implementation of the class : Transpose
"""

from .transformation import Transformation
from ..table import Table

class Transpose(Transformation): 
    """To transpose tables

    Parameters
    ----------
    new_headers : Iterable[str]
        The header of the transposed table

    Attributes
    ----------
    new_headers : Iterable[str]
        The header of the transposed table
    """
    def __init__(self, new_headers = [str]): 
        """constructor

        Parameters
        ----------
        new_headers : Iterable[str]
            The header of the transposed table
        """
        self.new_headers = new_headers
        
    def run(self, data : Table)-> Table : 
        """Perform the transposition of the table

        Does not modify the table given as parameter

        Parameters 
        ----------
        data : Table
            Table to be transposed.

        Returns
        -------
        Table
            The transposed table.
        """

        new_data = [data.headers]
        for variable in data.headers:
            row = data.col(variable)
            new_data.append([row])
        return Table(self.new_headers, new_data)

    def __str__(self)-> str : 
        """string representation of the transpose operation

        Returns 
        -------
        str : 
            The representation
        """
        return "The table has been transposed"        