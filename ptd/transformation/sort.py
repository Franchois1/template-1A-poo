"""module ptd.transformation.sort

Definition of class SortBy
"""

from operator import itemgetter

from ..table import Table
from .transformation import Transformation

class SortBy(Transformation):
    """To sort data table 

    Uses the builtin function sorted 

    Parameters
    ----------
    var : str
        the variable to use as key to sort the table

    Attributes
    ----------
    var : str
        the variable to use as key to sort the table
    """

    def __init__(self, var:str, descending: bool = False):
        """Constructor

        Parameters
        ----------
        var : str
            the variable to use as key to sort the table
        """
        self.var = var
        self.descending = descending
    
    def run(self, input: Table) -> Table:
        """To execute the sorting on the input table

        If var is not in the headers of the table given as parameter, 
        an IndexError is raised

        Parameters
        ----------
        input : Table
            Table to be sorted

        Returns
        -------
        Table
            Sorted table
        """
        if not self.var in input.headers:
            raise IndexError("{} is not a variable of table".format(self.var))
        
        # we get the index of the column 
        index = input.headers.index(self.var)
        return Table(input.headers, 
            sorted(input, key = itemgetter(index), reverse=self.descending))
    
    def __str__(self)-> str:
        """String representation of the operation

        Returns
        -------
        str
            The representation
        """
        return f'Sort table by variable : {self.var}'
