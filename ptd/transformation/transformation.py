"""module ptd.transformation.transformation

Definition of abstract class Transformation
"""

from abc import abstractmethod
from ..table import Table
from ..operation import Operation

class Transformation(Operation):
    """Abstract class to define Transformation operations 
    """

    @abstractmethod
    def __str__():
        pass
    