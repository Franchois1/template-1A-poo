"""module ptd.transformation.apply

Definition of class ApplyToColomn
"""

import copy

from .transformation import Transformation
from ..table import Table

class ApplyToColomn(Transformation):
    """To apply a function to a column

    Attributes 
    ----------
    var : Iterable[str]
        column(s) on wich apply the function
    function : function
        function to apply to the selected colomn(s)
    
    Parameters
    ----------
    var : Iterable[str]
        column(s) on wich apply the function
    function : function
        function to apply to the selected colomn(s)
    """
    def __init__(self, var: [str], function):
        """constructor

        Parameters
        ----------
        var : Iterable[str]
            colomn(s) on wich apply the function
        function : function
            function to apply to the selected colomn(s)
        """

        self.var = var
        self.function = function
    
    def run(self, data:Table) -> Table:
        """Run the operation 

        apply function to column(s)
        If applying the function raises an exception, it is excepted and data 
        is set to None

        Parameters
        ----------
        data : Table
            The table to modify

        Returns
        -------
        Table
            The modified table
        """
        out_table = copy.deepcopy(data)        
        for variable in self.var:
            if variable in out_table.headers :
                index = out_table.headers.index(variable)
                for row in out_table:
                    try:
                        row[index] = self.function(row[index])
                    except :
                        row[index] = None
        return out_table
        
    def __str__(self) -> str:
        """Describes what has been done

        Returns
        -------
        str
            a sentence describing the established changes
        """
        return "Apply {} to {}".format(self.function, self.var)
