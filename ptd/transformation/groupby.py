"""module ptd.transformation.groupby

Definition of the class : GroupBy
"""

import copy

from .transformation import Transformation
from ..table import Table
from ..toolBox import ToolBox
from .sort import SortBy

class GroupBy(Transformation):
    """To make grouping according to a variable

    Attributes 
    ----------
    var : str 
        specifies on which variable to perform the grouping
    
    Parameters
    ----------
    var : str 
        specifies on which variable to perform the grouping
    """
    def __init__(self, var:str, agregation = ToolBox.mean): 
        """constructor

        Parameters
        ----------
        var : str 
            specifies on which variable to perform the aggregation

        agregation : function
            function to agregate other data. This function should take a list 
            as parameter and return a value (like 'sum')
        """
        self.var=var
        self.agregation = agregation

    def run(self, data:Table)-> Table : 
        """perform the aggregation 

        If var is not in the headers of the table given as parameter an 
        IndexError is raised


        Parameters 
        ----------
        data : Table 
            Table on which the groupings have to be made 

        Returns
        -------
        Table 
            Table after the groupings have been made 
        """

        if not self.var in data.headers:
            raise IndexError("{} is not a variable of table".format(self.var))
        sorted_table = SortBy(self.var).run(data)
        # check if variable given is in the table and sort the table by it

        key_index = sorted_table.headers.index(self.var)
        # index of the column to group by

        var_to_group = sorted_table.headers[:]
        del(var_to_group[key_index])
        # the other variables 

        output_data = []
        i=0
        while i < len(sorted_table):
            key_value = sorted_table[i][key_index]

            row =  [[]]
            for j in range(len(sorted_table.headers)-1):
                row.append([])
            #row is a list of empty lists 
            
            row[key_index] = key_value 

            while (i < len(sorted_table) 
                and key_value == sorted_table[i][key_index]):
                for variable in var_to_group:
                    var_index = sorted_table.headers.index(variable)
                    row[var_index].append(sorted_table[i][var_index])
                i += 1

            
            for variable in var_to_group:
                var_index = sorted_table.headers.index(variable)
                row[var_index] = self.agregation(row[var_index])
            output_data.append(row)

        return Table(sorted_table.headers, output_data)


    def __str__(self) -> str:
        """String representation of the operation

        Returns
        -------
        str
            the representation
        """
        return "Grouping data by : {}".format(self.var)
        