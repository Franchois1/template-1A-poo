"""module ptd.transformation.map

Definition of class Map
"""

from ..table import Table
from .transformation import Transformation

class Map(Transformation):
    """to create a map with data

    Parameters
    ----------
    var : str
        variable of the table to plot in a map
    path : str
        path and name of picture where to save the map

    Attributes
    ----------
    var : str
        variable of the table to plot in a map
    path : str
        path and name of picture where to save the map
    """

    def __init__(self, var: str, path : str):
        """Constructor

        Parameters
        ----------
        var : str
            variable of the table to plot in a map
        path : str
            path and name of picture where to save the map
        """
        self.var = var
        self.path = path

    def run(self, input_table: Table) -> Table:
        """to perform the operation

        Uses map method of input_table

        Parameters
        ----------
        input_table : Table
            the table conaining the data to map

        Returns
        -------
        Table
            input_table unmodified
        """
        input_table.map(var=self.var, path=self.path)
        return input_table

    def __str__(self)->str:
        """String representation of the operation

        Returns
        -------
        str
            the representation
        """
        return f"Mapping {self.var} at : {self.path}"