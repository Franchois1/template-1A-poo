"""module ptd.transformation.eLoad

definition of class InternetLoad
"""

import os
import urllib.request

from ..table import Table
from .transformation import Transformation
from .load import Load

class InternetLoad(Transformation):
    """To load data from internet

    Parameters
    ----------
    source : str
        Shall either be 'incid_rea', 'stock', 'flux' 'age',  or 'services'
    
    Attributes
    ----------
    source : str
        a short name for the source 
    url : str
        link to the data
    """
    
    def __init__(self, source:str):
        """Constructor

        Parameters
        ----------
        source : str
            Shall either be 'incid_rea', 'stock', 'flux' 'age',  or 'services'
        """
        if source == 'incid_rea':
            self.url = "https://www.data.gouv.fr/fr/datasets/r/a1466f7f-4ece-4158-a373-f5d4db167eb0"
        elif source == 'stock':
            self.url = "https://www.data.gouv.fr/fr/datasets/r/63352e38-d353-4b54-bfd1-f1b3ee1cabd7"
        elif source == 'flux':
            self.url = "https://www.data.gouv.fr/fr/datasets/r/6fadff46-9efd-4c53-942a-54aca783c30c"
        elif source == 'age':
            self.url = "https://www.data.gouv.fr/fr/datasets/r/08c18e08-6780-452d-9b8c-ae244ad529b3"
        elif source == 'services':
            self.url = "https://www.data.gouv.fr/fr/datasets/r/41b9bd2a-b5b6-4271-8878-e45a8902ef00"
        else :
            raise AttributeError("'{}' is not a source".format(source))
        self.source = source

    def run(self, data:Table=None) -> Table:
        """perform the download

        uses urllib.request.urlopen to get the data from the url
        write this data as csv 
        uses ptd.transformation.load.Load to read data from this csv file
        remove csv file
        return the Table

        Parameters
        ----------
        data : Table, optional
            Unused parameter, by default None

        Returns
        -------
        Table
            output table 
        """
        with urllib.request.urlopen(self.url) as response:
            online_data = response.read()
        with open("tmp.csv", 'wb') as tmp_file:
            tmp_file.write(online_data)
        out_table = Load("tmp.csv").run(None)
        os.remove("tmp.csv")
        return out_table
    
    def __str__(self) -> str:
        """string representation of the operation

        Returns
        -------
        str
            the representation
        """
        return "Load data from data.gouv.fr : {}".format(self.source)