"""module ptd.transformation.importjson 

Definition of class ImportJSON
"""

import os
import csv
import json

from ..table import Table
from .transformation import Transformation
from .load import Load


class ImportJSON(Transformation):
    """to import table form json files as table

    """
    
    def __init__(self, path:str, table_name: str):
        """constructor

        If path doesn't lead to a JSON file, a ValueError is raised

        Parameters
        ----------
        path : str
            path to the JSON file
        table_name : str
            name of the table in the JSON to import
        """
        if path[-4:] != "json":
            raise ValueError(f"This file is not a JSON : {path}")
        self.path = path
        self.table_name = table_name
    
    def run(self, input_table: Table) -> Table:
        """run the import

        Parameters
        ----------
        input_table : Table
            unused argument

        Returns
        -------
        Table
            table made out of the json file
        """

        with open(self.path) as jf:
            json_content = json.load(jf)

        #extracting table
        json_table = json_content[self.table_name]

        #getting headers 
        tmp = json_table[0]
        fields = list(tmp.keys()) 

        with open("tmp.csv", mode="w", newline='') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=fields, 
                            delimiter=';', quotechar='"')
            head_writer = csv.writer(csv_file, delimiter=';', quotechar='"')
            
            head_writer.writerow(fields)
            writer.writerows(json_table)
        
        output = Load("tmp.csv").run(None)
        os.remove("tmp.csv")
        return output

    def __str__(self) -> str:
        """String representation of the operation

        Returns
        -------
        str
            The reprensentation
        """
        return f"Import {self.table_name} from JSON : {self.path}"
