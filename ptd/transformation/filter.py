"""module ptd.transformation.filter

Definition of class Filter
"""

from .transformation import Transformation
from ..table import Table
from ..toolBox import ToolBox


class Filter(Transformation):
    """To filter a table on select variables/data 

    Attributes
    ----------
    var : str
        Variable on which the filter is focused
    criterion : boolean function
        criterion to select individuals (rows)

    Parameters 
    ----------
    var : str
        Variable on which the filter is focused
    criterion : boolean function
        criterion to select individuals (rows)
    """

    def __init__(self, var:str, criterion : bool ): 
        """Constructor

        Parameters 
        ----------
        var : str
           Variable on which the filter is focused
        criterion : boolean function
           criterion to select individuals (rows)
        """
        self.var=var
        self.criterion=criterion

    def run(self, data : Table) -> Table : 
        """perform the filter on the table

        Does not modify the Table given as parameter

        Parameters
        ----------
        data : Table 
            Table to be filtered
        
        Returns 
        -------
        Table
            Filtered table
        """
        output_data = []
        num_var = data.headers.index(self.var)
        for ind in data : 
            if self.criterion(ind[num_var]): 
                output_data.append(ind[:])
        return Table(data.headers, output_data)
            
            

    def __str__(self)-> str : 
        """String description of the operation

        Returns
        -------
        str     
            the desccription
        """
        return f"Filter on the following variable : '{self.var}'"
