"""module ptd.transformation.load

Definition of class Load
"""

import csv

from ..table import Table
from .transformation import Transformation

class Load(Transformation):
    """To load csv files    

    Parameters
    ----------
    path : str
        Path to the csv file

    Attributes
    ----------
    path : str
        Path to the csv file
    """
    
    def __init__(self, path: str):
        """Constructor

        Parameters
        ----------
        path : str
            Path to the csv file
        """
        self.path = path


    def run(self, data: Table)->Table:
        """read a csv and returns a Table object

        uses csv.reader, 
        delimiter is semi-colon ";" , 
        must contain hearders

        Parameters
        ----------
        data : Table 
            unused

        Returns
        -------
        Table
            Table object containing data from the file
        """
        data = []
        headers = []
        with open(self.path) as csvfile:
            lines = csv.reader(csvfile, delimiter=';', quotechar='"')
            first = True
            for ind in lines:
                if first:
                    headers = ind
                    first=False
                    continue #go to next iteration 
                data.append(ind)
        return Table(headers, data)
        
    def __str__(self) -> str:
        """String representation of the operation

        Returns
        -------
        str
            The representation
        """
        return f"Load data from : \"{self.path}\""