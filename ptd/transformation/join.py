"""module ptd.transformation.join

Definition of class Join
"""

from ..table import Table
from .transformation import Transformation
from .select_var import SelectVar

class Join(Transformation):
    """To join tables 
    
    Type of join can be specified and the variables to be matched too.

    Attributes
    ----------
    data2: Table 
        Table to be joined.
    type : str 
        specifies the type of join 
    var1: str 
        variable of table 1 from which to perform the join
    var2 :str 
        variable of table 2 from which to perform the join

    Parameters 
    ----------
    data2: Table 
        Table to be joined.
    type : str 
        specifies the type of join 
    var1: str 
        variable of table 1 from which to perform the join
    var2 :str 
        variable of table 2 from which to perform the join

    """

    def __init__(self, data2 : Table, var1 : str, var2 : str = None, 
                type : str = "inner"):
        """Constructor

        Raise a ValueError if type is not "inner", "right", "left" or "full"

        Parameters
        ----------
        data : Table
            The data
        var1 : str
            the first variable uses for the join
        var2 : str
            the second variable uses for the join
        type : str
            the way to join the tables 
            either "inner", "right", "left" or "full"
        """
        self.data2 = data2
        self.var1 = var1

        if not type in ["inner", "left", "right", "full"]:
            raise ValueError(f"Unrecognize type of join : {type}\n"+
                        "type should be 'inner', 'right', 'left' or 'full'")
        else:
            self.type = type

        if var2 == None:
            self.var2 = var1
        else :
            self.var2 = var2

        

    def run(self, data1 : Table) -> Table:
        """Perform the join 

        Does not modify the tables given as parameter

        Parameters
        ----------
        data1 : Table
            
            the left table of the join
        """
        try:
            key1 = data1.headers.index(self.var1)
        except ValueError:
            raise IndexError(f"{self.var1} is not a variable of left table")
        try:
            key2 = self.data2.headers.index(self.var2)
        except ValueError:
            raise IndexError(f"{self.var2} is not a variable of right table")

        key_col1 = data1.col(self.var1)
        key_col2 = self.data2.col(self.var2)

        out_data = []

        for row1 in data1:

            #matching lines : (all types of join concerned)
            if row1[key1] in key_col2:
                for row2 in [row2 for row2 in self.data2 if row2[key2] == row1[key1]] :
                    out_data.append(row1+row2)
            
            #left and full joins : adding the unmatching rows of left table
            # to the output
            elif self.type in ["left", "full"]:
                out_data.append(row1 + [None]*len(self.data2.headers))
        
        #rigth and full joins : addinf the unmatching rows of right table to 
        # the output
        if self.type in ["right", "full"]:
            for row2 in [row2 for row2 in self.data2 if not row2[key2] in key_col1]:
                row = [None]*len(data1.headers)+row2
                #adding the key_value to the key column on the left part
                #because right key column will be deleted
                row[key1] = row2[key2]

                out_data.append(row)

        #creating the out table
        out_headers = data1.headers + self.data2.headers
        out_table = Table(out_headers, out_data)

        #deleting common column in the right part and return table
        #  = select all but this column
        right_header = self.data2.headers[:] #avoid modifying the input table
        del(right_header[key2])
        out_headers = data1.headers + right_header
        return SelectVar(out_headers).run(out_table)

    def __str__(self)-> str : 
        """String representation of the operation

        Returns
        -------
        str
            the representation
        """
        return f"Join tables on keys : '{self.var1}' and '{self.var2}'"
