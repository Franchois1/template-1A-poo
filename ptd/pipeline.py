"""module ptd.pipeline

definition of class 'Pipeline'
"""

from .table import Table
from .operation import Operation

class Pipeline(list):
    """Pipeline of multiple operations

    Contains a list of operations to chain 
    Note that a pipeline can contain an other pipeline
    Indeed Pipeline class defines a run method

    Methods
    -------
    run(data: Table) -> Table:
        run the pipeline with 'data' as input Table
    """

    def run(self, data: Table=None) -> Table:
        """run the pipeline

        chains the operations and returns a table 

        Parameters
        ----------
        data : Table
            Table on wich apply the operations

        Returns
        -------
        Table
            Table on wich the operations have been applied
        """
        for step in self : 
            data = step.run(data)
        return data

    def __add__(self, other: Operation):
        """add an operation (or a pipeline) to the pipeline 

        If 'other' is not a pipeline nor an operation a TypeError is raised

        Parameters
        ----------
        other : Operation or Pipeline 
            the object to add to the pipeline

        Returns
        -------
        Pipeline
            The extended pipeline
        """
        
        if not (isinstance(other, Pipeline) or isinstance(other, Operation)):
            raise TypeError("Expecting Pipeline or Operation object "+
                            f"where '{repr(other)}' was given")
        
        result = self[::] #not a pipeline 
        result.append(other) #only a list
        return Pipeline(result)
    
    def __str__(self) -> str:
        """string represntation of the pipeline

        One line for each operation in the pipeline

        Returns
        -------
        str
            the string representation
        """
        
        output = ""
        i = 0
        for step in self:
            if isinstance(step, Operation):
                output += "{} : {:88.88}\n".format(i, str(step))
            else : # if step is a pipeline
                output += "{} : pipeline : print this step for more info\n".format(i)
            i += 1
        return output
        