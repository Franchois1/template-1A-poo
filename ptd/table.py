"""module ptd.table

definition of class 'Table'
"""

import csv
from datetime import date

from mapper import CartoPlot

class Table():
    """Data table

    Basic data frames 

    Attributes
    ----------
    headers : 
        Name of the variables of the Data
    data :
        data frame containing the data 

    Parameters 
    ----------
    headers : 
        Name of the variables of the Data
    data :
        data frame containing the data
    """
    
    def __init__(self, headers, data):
        """create

        Parameters
        ----------
        headers : iterable[str]
            header of the table, ie names of variables
        data :
            data frame
        """

        if len(data[0]) != len(headers):
            raise IndexError("Length of headers does not match length of data")
        self.headers = headers
        self.data = data

    def __len__(self)-> int:
        """returns the length of the table

        return the lenght of data, ie the number of individuals

        Returns
        -------
        int
            lenght of table

        Examples
        --------
        >>> my_table = Table(headers = ["a", "b", "c"],
        ...                 data = [[1.78, 60, "H"],
        ...                         [1.67, 58, "F"],
        ...                         [1.87, 70, "H"]])
        >>> len(my_table)
        3 
        """

        return len(self.data)
    
    def __getitem__(self, key):
        """to get individuals

        Parameters
        ----------
        key : slice
            slice corresponding to the index of the lines to be selected

        Examples
        --------
        >>> my_table = Table(headers = ["a", "b", "c"],
        ...                 data = [[1.78, 60, "H"],
        ...                         [1.67, 58, "F"],
        ...                         [1.87, 70, "H"]])
        >>> my_table[1]
        [1.67, 58, "F"]
        """

        return self.data[key]

    def col(self, var:str):
        """return the column of 'var'

        The column is returned as a list. If 'var' is not a column name, an 
        empty column is returned

        Parameters
        ----------
        var : str
            name of the variable ie name of the column

        Returns
        -------
        column : iterable
            The selected column (might be empty) as a list
        """

        column = []
        if not var in self.headers:
            return column
        else :
            index = self.headers.index(var)
            for row in self.data:
                column.append(row[index])
        return column

    def __str__(self) -> str:
        """string reprensentation of a Table

        Table style with headers and max 10 rows of the Table

        Returns
        -------
        str
            representation of table
        """

        m = '+' + '-'*(len(self.headers)*13-1) + '+\n|'
        for var in self.headers:
            m += '{:^12.12}|'.format(var)
        m += '\n+' + '-'*(len(self.headers)*13-1) + '+'
        
        for i in range(min(len(self), 10)):
            m += '\n|'
            for cell in self.data[i]:
                m += "{:^12.12}|".format(str(cell))
        m += '\n+' + '-'*(len(self.headers)*13-1) + '+'
        
        i += 1
        if i < len(self):
            m += "\n... {} more rows".format(len(self)-i)
        return m

    def save_as_csv(self, path: str):
        """Save table in a csv file

        If file given in path exist, it is overwritten
        Else it is created

        Parameters
        ----------
        path : str
            path to the destination csv file
        """
        with open(path, mode="w", newline='') as csv_file:
            writer = csv.writer(csv_file, delimiter=';', quotechar='"', 
                                quoting=csv.QUOTE_NONNUMERIC)
            writer.writerow(self.headers)
            writer.writerows(self)

    def map(self, var: str, path: str):
        """to create a map for variable var

        Map wil be save in the fille given in path 
        Uses CartoPlot class to build the map

        Parameters
        ----------
        var : str
            variable of the table to plot in a map
        path : str
            path and name of picture where to save the map
        """
        zone = []
        var_data = []
        dep_map = False
        cp = CartoPlot()

        if "dep" in self.headers:
            zone = self.col("dep")
            dep_map = True
        elif "reg" in self.headers:
            zone = self.col("reg")
        elif "numReg" in self.headers:
            zone = self.col("numReg")
        else :
            raise IndexError("No geographic information in table : {self.headers}")
        
        if len(zone) != len(set(zone)): #check if each element is unique
            raise IndexError("Multiple data for the same area")

        if not var in self.headers:
            raise ValueError(f"{var} is not a variable of the table")
        else : 
            var_data = self.col(var)

        if len(zone) != len(var_data):
            raise IndexError("Number of labels doesn't match number of data")
    
        data_to_map = {}
        for i in range(len(zone)):
            data_to_map[zone[i]] = var_data[i]

        if dep_map:
            fig = cp.plot_dep_map(data=data_to_map, show_name=False,
                                 x_lim=(-6, 10), y_lim=(41, 52))
        else :#map of Régions
            fig = cp.plot_reg_map(data=data_to_map, show_name=False,
                                 x_lim=(-6, 10), y_lim=(41, 52))
        fig.savefig(path)