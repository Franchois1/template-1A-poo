"""module ptd.estimation.moving_average

Definition of class : MovingAverage
"""

import copy

from .estimation import Estimation
from ..table import Table
from ..toolBox import ToolBox

class MovingAverage(Estimation):
    """to compute moving average on data

    Parameters
    ----------
    var : Iterable[str]
        the variables on wich to compute moving averages
    period : int
        period of the average

    Attributes
    ----------
    var : Iterable[str]
        the variables on wich to compute moving averages
    period : int
        period of the average
    """

    def __init__(self, var: [str], period : int):
        """Constructor

        Parameters
        ----------
        var : list[str]
            Variables on which to calculate an average
        period : int
            period over which to calculate the average
        """
        super().__init__(var)
        self.period = period 
    
    def run(self, input_table : Table) -> Table:
        """Compute the moving average

        Does not modify input_table
        The output table has a new column added containing the moving average

        Parameters
        ----------
        input_table : Table
            data table on wich to compute the moving average

        Returns
        -------
        Table
            table with moving average column added
        """
        out_table = copy.deepcopy(input_table)
        for variable in self.var:
            if not variable in out_table.headers:
                continue
            moving_average = ToolBox.moving_average(out_table.col(variable), 
                                                    self.period)
            for i in range(self.period-1):
                moving_average.append(None)

            out_table = ToolBox.new_col(out_table, 
                                        f"MA_{variable}",
                                        moving_average )   
        return out_table

    def __str__(self)->str:
        """String representation of the operation

        Returns
        -------
        str
            the representation
        """
        return (f"Compute moving average (period={self.period}) " +
                f"on  : '{', '.join(self.var)}'")