"""module ptd.estimation.mean

Definition of the class : Mean
"""

from .estimation import Estimation
from ..table import Table
from ..toolBox import ToolBox

class Mean(Estimation) : 
    """To compute an average over selected items in the table.

    Attributes
    ----------
    var : Iterable[str]
        the variables on wich mean have to be computed

    """

    def run(self, data : Table) : 
        """perform the computing of means

        Does not modify the table gien as parameter
        
        Parameters
        ----------
        data : Table
            Table from which calculations are to be made   

        Returns
        -------
        table_mean : Table 
            table containing the means  

        Example 
        -------
        >>> my_table = Table(headers = ["a", "b", "c"],
        ...                 data = [[1.78, 60, "H"],
        ...                         [1.67, 58, "F"],
        ...                         [1.87, 70, "H"]])
        >>> m = Mean("b").run(my_table)
        >>> print(m)
        +------------+
        |     b      |
        +------------+
        |62.666666666|
        +------------+
        """

        name_var = []
        datas = []
        for variable in self.var :
            if not variable in data.headers:
                name_var.append(variable)
                datas.append(None)            
            else :
                name_var.append(variable)
                datas.append(ToolBox.mean(data.col(variable)))
        
        return Table(name_var, [datas])
        
    def __str__(self) :
        """ Find out which calculations have been made on which variables 
        
        Returns 
        -------
        str : 
            a sentence describing the established changes

        Example 
        -------
        >>> m1=Mean(["Number of deaths in the last 24 hours"])
        >>> print(m1)
        "Compute the average on : 'Number of deaths in the last 24 hours'"
        >>> m2=Mean(["Age", "Number of children"])
        >>> print(m2)
        "Compute the average on: 'Age', 'Number of children'"
        """
        return f"Compute the average of  : '{', '.join(self.var)}'"
