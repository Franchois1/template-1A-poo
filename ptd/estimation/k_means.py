"""module ptd.estimation.k_means

Definition of the class : K_means
"""

import math
import copy
from random import randint

from ..toolBox import ToolBox
from ..table import Table
from .estimation import Estimation

class K_means(Estimation) : 
    """Allows the selected data to be grouped into n groups.
   
    Parameters
    ----------
    n_classes : int
        number of groups to be established

    Attributes
    ----------
    var : Iterable[str]
        data to be aggregated

    n_classes : int
        number of groups to be established

    Examples
    --------
    
    """
    
    def __init__(self, var, n_classes, valeur_max_iter=30): 
        """Constructor

        Parameters
        ----------
        var : iterable[str]
            data to be aggregated

        n_classes : int
            number of groups to be established
        
        valeur_max_iter : int 
            maximum number of iterations in case of non convergence
        """

        super().__init__(var) 
        self.n_classes = n_classes
        self.valeur_max_iter=valeur_max_iter

    def run(self, data : Table) : 
        """Outputs a new table with the established groupings.

        An exception is raised if n_classes is supperior to the number of 
        rows in the table given as parameter

        Parameters
        ----------
        data : Table
            table on which the groupings are to be made

        Returns 
        -------
        Table
            Adds a column to the input table, specifying the group to 
            which the individuals belongs
        """
        
        #Creation of the table on which groups have to be made
        name_var = []
        individual=[]
        for i in range (len(data)):
            individual.append([])
        for variable in self.var :
            if not variable in (data.headers):
                name_var.apppend(None)
            else :
                name_var.append(variable)
                for num_ind in range (data.__len__()):
                    individual[num_ind].append(
                            data.data[num_ind][data.headers.index(variable)])     
        table_to_group = Table(name_var, individual)
        
       
        #Exception
        if not 1<= self.n_classes <= (len(table_to_group)):
            raise Exception("Number of classes impossible")


        #Creation of the centroid:
        centroid =[]
        for i in range (self.n_classes):
            centroid.append(table_to_group.data[i])
        #randint(0, table_to_group.__len__() -1)
        
        #creation of the table indicating the class to which each individual
        #belongs
        table_grouped=copy.deepcopy(table_to_group)
        for i in range (table_grouped.__len__()):
            table_grouped.data[i].append(0)
        table_grouped.headers.append("Class")

        #K_means algorithm: 
        compteur = 0 
        while compteur < self.valeur_max_iter:       
            partition=[]
            for i in range (self.n_classes): 
                partition.append([])

            #each individual is assigned to a class according to the 
            #distance to centroids
            for num_row in range (len(table_to_group)) : 
                distance = 10**10
                for i in range (len(centroid)):
                    if ToolBox.euclidian(table_to_group.data[num_row],centroid[i])<distance:
                        distance = ToolBox.euclidian(table_to_group.data[num_row],centroid[i])
                        num_class = i 
                table_grouped.data[num_row][len(table_grouped.headers)-1] = num_class
                partition[num_class].append(table_to_group.__getitem__(num_row))
    
            #new centroid 
            new_centroid=[]
            #for i in range (self.n_classes):
            #    new_centroid.append([])

            for num_partition in range (len(partition)): 
                mean_part_i=[]
                for num_variables in range (len(table_to_group.headers)):
                    colonne=[]
                    for num_ind in range (len(partition[num_partition])):
                        colonne.append(partition[num_partition][num_ind][num_variables])
                    mean_part_i.append(ToolBox.mean(colonne))
                new_centroid.append(mean_part_i)
            
            
            #Check the convergence
            if centroid == new_centroid :
                break
            else : 
                centroid=new_centroid
            #incrementation of the counter
            compteur = compteur +1

        #Adding the non selected variable for the K_means to the returned table
        return ToolBox.new_col(data, 'Class', table_grouped.col('Class'))

    def __str__(self) : 
        """string representation of the operation

        Returns 
        -------
        str : 
            the representation

        """
        return f"k-means of {self.n_classes} groups"

    

    