"""module ptd.

definition of class Variance

by Franchois, Emilitaire and Saraquette
"""

from .estimation import Estimation
from ..table import Table
from ..toolBox import ToolBox

class Variance(Estimation): 
    """To compute variances on selected variables.
    
    """

    def run(self, data : Table) -> Table :
        """Run and compute the variance

        Parameters
        ---------
        data : Table
            Table from which calculations are to be made   

        Returns
        -------
        table_variance : Table 
            New table containing the variances
        """

        name_var = []
        datas = []
        for variable in self.var :
            if not variable in data.headers:
                name_var.append(variable)
                datas.append(None)            
            else :
                name_var.append(variable)
                datas.append(ToolBox.variance(data.col(variable)))
        
        return Table(name_var, [datas])

        
    def __str__(self): 
        """string decription 

        Returns 
        -------
        str : 
            the description

        Example 
        -------
        >>> m1=Mean(["Number of deaths in the last 24 hours"])
        >>> __str__(m1)
        'Compute the variance on : Number of deaths in the last 24 hours'
        >>> m2=Mean(["Age", "Number of children])
        >>> __str__(m2)
        'Compute the variance on : Age, Number of children'
        """

        return(f"Compute the variance on  : '{', '.join(self.var)}'")


        