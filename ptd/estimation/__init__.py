"""subpackage ptd.estimation

This sub-package defines the abstract class estimation and its subclasses
An estimation is an operation(superclass) wich is meant to compute 
statistical value (eg a mean)

When ptd package is imported trough 'from ptd import *' the following
names are defined :
 - K_means
 - Mean
 - MovingAverage
 - Variance

Modules
-------
k_means
    contains class K_means
mean
    contains class Mean
movingaverage
    contains class MovingAverage
variance
    contains class Variance
"""

from .k_means import K_means
from .mean import Mean
from .movingaverage import MovingAverage
from .variance import Variance

__all__ = [
    "K_means",
    "Mean",
    "MovingAverage",
    "Variance"
]