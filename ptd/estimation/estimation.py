"""module ptd.estimation.estimation

definition of the class : Estimation
"""

import ptd
from ptd.operation import Operation
from abc import abstractmethod

class Estimation(Operation):
    """abstract class Estimation to define Estimation operations

    Parameters
    ----------
    var : Iterable[str]
        variables that must undergo statistical calculations

    Attributes
    ----------
    var : Iterable[str]
        variables that must undergo statistical calculations
    """
    def __init__(self, var):
        """Constructor
            
        Parameters
        ----------
        var : Iterable[str]
            variables that must undergo statistical calculations
        """
        self.var=var

    @abstractmethod
    def __str__(): 
        pass


