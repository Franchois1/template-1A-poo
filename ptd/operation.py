"""module ptd.operation

definition of abstract class 'Operation'
"""

from abc import ABC, abstractmethod
from .table import Table

class Operation(ABC):
    """Elementary operation for pipelines

    Abstract class to create classes of elementary operation
    This implementation ensure sub-classes have the correct methods
    """

    @abstractmethod
    def __init__(self):
        """constructor
        """
        pass
    
    @abstractmethod
    def run(self, data: Table) -> Table:
        """to run the operation

        parameter and return are Table for the pipeline implementation

        Parameters
        ----------
        data : Table
            Table on wich apply the operation

        Returns
        -------
        Table
            Table on wich the operation has been applied
        """
        pass
    
    @abstractmethod
    def __str__(self) -> str:
        """string representation of the operation

        Return should be a single line description

        Returns
        -------
        str
            reprensentation of the operation
        """
        pass