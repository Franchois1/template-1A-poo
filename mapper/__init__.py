"""package map

Provide Cartoplot class to map data
"""

from .cartoplot import CartoPlot

__all__=["CartoPlot"]